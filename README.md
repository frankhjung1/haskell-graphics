# Graphics Example Using Gloss

Work through examples and code from [The Haskell School of
Expression](https://www.cambridge.org/core/books/haskell-school-of-expression/70651D70E17ECC07C91D8487D2EFEAE7).


Gloss coordinate system for window size (xWin, yWin) is:

- Window top left = (0, 0)
- Window bottom right (xWin - 1, yWin - 1)

## Issues

### user error (unknown GLUT entry glutInit)

When running the program, the following error is displayed:

```text
graphics: user error (unknown GLUT entry glutInit)
```

On Linux you are missing the openGL package. Install the package
[freeglut3-dev](https://freeglut.sourceforge.net/) using your package manager.

## References

- [Gloss Documentation](https://hackage.haskell.org/package/gloss/docs/Graphics-Gloss.html)
  - [API](http://ztellman.github.io/gloss/index.html)
  - [Graphics.Gloss](https://hackage.haskell.org/package/gloss/docs/Graphics-Gloss.html)
  - [Graphics.Gloss.Data.Picture](https://hackage.haskell.org/package/gloss/docs/Graphics-Gloss-Data-Picture.html)
- [Gloss Package](https://hackage.haskell.org/package/gloss)
- [Graphics with Gloss](https://sodocumentation.net/haskell/topic/5570/graphics-with-gloss)
