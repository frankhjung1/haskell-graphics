{-|

Module      : Main
Description : Run a Gloss graphics example.
Copyright   : © Frank Jung, 2020-2024
License     : BSD

A simple graphics example using the Gloss from
<https://sodocumentation.net/haskell/topic/5570/graphics-with-gloss
Graphics with Gloss>.

A simple graphics example using the Gloss from
<https://sodocumentation.net/haskell/topic/5570/graphics-with-gloss Graphics with Gloss>.

For package documentation, see
<https://hackage.haskell.org/package/gloss Hackage: Gloss>

-}

module Main (main) where

import           Graphics.Gloss
import qualified Shape          as S

-- | Draws a circle in a small window with a white background.
main :: IO ()
main = display window background drawing
    where
      window = InWindow "Example" (300, 300) (10, 10)
      background = white
      drawing = pictures
        [ color red $ circleSolid 80
        , color green $ S.shapeToPicture (S.Rectangle 100 100)
        , translate (-35) 0 $ color blue $ scale 0.1 0.1 $ text "Hello World"
        ]
