{- |

  Module      : Shape
  Description : Simple shapes for use in graphics.
  Copyright   : © Frank Jung, 2020-2024
  License     : BSD-3
  Maintainer  : frankhjung@linux.com
  Stability   : education
  Portability : Linux

-}

module Shape (
              Shape(Rectangle, Ellipse, RightTriangle, Polygon)
            , Radius, Side, Vertex, Point
            , xWin, yWin
            , toPixel, fromPixel, trans, transList
            , circle, rectangle, rightTriangle, square
            , distanceBetween, area
            , shapeToPicture
             ) where

import qualified Graphics.Gloss.Data.Picture as G

-- | Generic shapes.
data Shape = Rectangle Side Side
           | Ellipse Radius Radius
           | RightTriangle Side Side
           | Polygon [Vertex]
           deriving Show

-- | Foci of 'Ellipse'
type Radius = Float
-- | Sides of 'Rectangle' and 'RightTriangle'
type Side   = Float
-- | Vertices for 'Polygon'
type Vertex = (Float, Float)
-- | Point in graphics coordinate system.
type Point = (Int, Int)

-- | Default window size.
xWin :: Int
xWin = 1600

-- | Default window size.
yWin :: Int
yWin = 900

-- | Translate from graphics vertices to cartesian coordinate system
trans :: Vertex -> Point
trans (x, y) = (xWin `div` 2 + toPixel x, yWin `div` 2 - toPixel y)

-- | Translate a list of graphics vertices to cartesian coordinates.
transList :: [Vertex] -> [Point]
transList = map trans

-- | Convert from Pixel to coordinate system.
fromPixel :: Int -> Float
fromPixel n = intToFloat n / 100

-- | Convert an integer to a floating point value.
intToFloat :: Int -> Float
intToFloat n = fromInteger (toInteger n)

-- | Convert to Pixel from coordinate system.
toPixel :: Float -> Int
toPixel x = round (100 * x)

-- | Circle is an 'Ellipse' with equal foci.
circle :: Radius -> Shape
circle r = Ellipse r r

-- | reduce duplication
getVertices :: Side -> Side -> [Vertex]
getVertices s1 s2 = [v1, v2, v3, v4]
  where v1 = (0, 0) :: Vertex
        v2 = (s1, 0) :: Vertex
        v3 = (s1, s2) :: Vertex
        v4 = (0, s2) :: Vertex

-- | Rectangle is a quadrilateral polygon with four 90-degree angles.
rectangle :: Side -> Side -> Shape
rectangle s1 s2 = Polygon [v1, v2, v3, v4]
  where (v1:v2:v3:v4:_) = getVertices s1 s2

-- | RightTriangle is a three point polygon with a 90-degree angle.
rightTriangle :: Side -> Side -> Shape
rightTriangle s1 s2 = Polygon [v1, v2, v3]
  where (v1:v2:v3:_) = getVertices s1 s2

-- | Square is a 'Rectangle' where all 'Side's are equal length.
square :: Side -> Shape
square s = Rectangle s s

-- | Calculate area for a given 'Shape'. An alternate algorithm is this one:
-- <https://www.mathopenref.com/coordpolygonarea2.html Algorithm to find the area of a polygon>
area :: Shape -> Float
area (Rectangle s1 s2)      = s1 * s2
area (RightTriangle s1 s2)  = s1 * s2 / 2
area (Ellipse r1 r2)        = pi * r1 * r2
area (Polygon (v1:vs))      = polygonArea vs
  where
    -- Area of a polygon is sum of triangular segments.
    polygonArea :: [Vertex] -> Float
    polygonArea (v2:v3:vs') = triArea v1 v2 v3 + polygonArea (v3:vs')
    polygonArea _           = 0
area _                      = 0

-- | Function to calculate distance between two vertices.
distanceBetween :: Vertex -> Vertex -> Float
distanceBetween (x1, y1) (x2, y2) = sqrt((x1 - x2)**2 + (y1 - y2)**2)

-- | Area of triangular polygon segements.
triArea :: Vertex -> Vertex -> Vertex -> Float
triArea v1 v2 v3 = let a = distanceBetween v1 v2
                       b = distanceBetween v2 v3
                       c = distanceBetween v3 v1
                       s = (a + b + c) / 2
                    in sqrt(s * (s - a) * (s - b) * (s - c))

-- | Convert Shape to a 'Picture' graphic.
shapeToPicture :: Shape -> G.Picture
shapeToPicture (Rectangle s1 s2)   = G.rectangleSolid s1 s2
shapeToPicture (Ellipse _ _)       = undefined
shapeToPicture (RightTriangle _ _) = undefined
shapeToPicture (Polygon _)         = undefined
