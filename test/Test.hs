module Main(main) where

import           Shape           (Shape (Polygon, Rectangle, RightTriangle),
                                  Side, area, rectangle, rightTriangle, trans)
import           Test.Hspec      (describe, hspec, it, shouldBe)
import           Test.QuickCheck (Gen, arbitrary, forAll, getPositive,
                                  quickCheck)

tolerance :: Float
tolerance = 1e-10

genSides :: Gen (Side, Side)
genSides = do
  x <- getPositive <$> arbitrary
  y <- getPositive <$> arbitrary
  return (x, y)

prop_RectangleArea :: Side -> Side -> Bool
prop_RectangleArea a b = abs (areaRectangle - areaPolygon) <= tolerance
  where
    areaRectangle = area(rectangle a b)
    areaPolygon = area(Polygon[(0,0), (a,0), (a,b), (0,b)])

prop_TriangleArea :: Side -> Side -> Bool
prop_TriangleArea a b = abs (areaTriangle - areaPolygon) <= tolerance
  where
    areaTriangle = area(rightTriangle a b)
    areaPolygon = area(Polygon[(0,0), (a,0), (a,b)])

prop_ShapeArea :: Side -> Side -> Bool
prop_ShapeArea a b = area(Rectangle a b) == a * b

main :: IO ()
main = hspec $ do

  describe "test shape areas" $ do

    it "right triangle (3, 4) = polygon" $
      area(RightTriangle 3 4) `shouldBe` area(Polygon[(0, 0), (3, 0), (0, 4)])
    it "right triangle (3.652, 5.125) = 9.35825" $
      area(RightTriangle 3.652 5.125) `shouldBe` 9.35825
    it "polygon (0, 0) (3.652, 0), (0, 5.125) = 9.358252" $
      area(Polygon[(0, 0), (3.652, 0), (0, 5.125)]) `shouldBe` 9.358252
    it "trans (0, 0) = (xWin `div` 2, yWin `div` 2)" $
      trans (0, 0) `shouldBe` (800, 450)

  describe "quick check shape areas" $ do

    it "area of rectangle = area of equivalent polygon" $
      quickCheck $ forAll genSides $ uncurry prop_RectangleArea
    it "area of right triangle = area of equivalent polygon" $
      quickCheck $ forAll genSides $ uncurry prop_TriangleArea
    it "area of rectangle = area of Rectangle" $
      quickCheck $ forAll genSides $ uncurry prop_ShapeArea
